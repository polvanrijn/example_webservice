# Setup
## Local testing

Simply run `gunicorn app:app -b :5000 -t 90` in the folder.

## Remote setup

1. Setup the right EC2 instance (e.g. `m5.xlarge` for testing, `m5.4xlarge` for experiment), install Ubuntu 18.04
1. Make sure port 5000 is reachable
1. Connect to client with ssh
1. Upload files using ssh
1. Install pip3:
```
sudo apt update
sudo apt install python3-pip

# Upgrade pip
sudo pip3 install --upgrade pip
```
6. Install requirements
```
sudo pip install -r requirements.txt
```
7. Start the service (MAKE SURE THE WORKERS MATCH THE CPUS, in this case 64 CPUs)

```
tmux
gunicorn app:app --worker-tmp-dir /dev/shm --workers=64 -b :5000 -t 90
```

Hard reset
```
sudo kill -9 `sudo lsof -t -i:5000`
```

8. Double check if the connection works with `example_request.py`