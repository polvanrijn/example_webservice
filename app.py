from flask import Flask, request, send_file
import os
import logging
import io
import tempfile
from custom_synth import synth_stimulus

app = Flask(__name__)

if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)


@app.route('/generate', methods=["POST"])
def generate():
    app.logger.info("Synthesizing stimulus...")
    json = request.json
    assert json["password"] == "Some super strong password"
    assert "file" in json
    assert "vector" in json
    filename = 'output.wav'

    # Open a temporary folder for storing the output
    with tempfile.TemporaryDirectory() as out_dir:
        output_file = os.path.join(out_dir, filename) # Get the path
        synth_stimulus(json["vector"], output_file, {'file': json["file"]}) # Synthesize the stimulus
        with open(output_file, 'rb') as bites:
            # return as bytes
            return send_file(
                io.BytesIO(bites.read()),
                attachment_filename=filename,
                mimetype='audio/wav'
            )

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')