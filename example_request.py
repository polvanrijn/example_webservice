# Example request

import requests

url = "http://0.0.0.0:5000"  # server 1
try:
    response = requests.post(url + "/generate", json={
        "password": "Some super strong password",
        "vector": [
            1.5,  # 1. Duration, percent
            0.1,  # 2. Tremolo rate, st
            0.05,  # 3. Tremolo depth, dB
            0,  # 4. Shift, semitones
            1,  # 5. Range, percent
            0,  # 6. Increase/Decrease, semitones
            0  # 7. Jitter, custom unit
        ],
        "file": 'Harvard_L35_S01_0.wav',
    })

    audio_out = response.content
    output_path = 'test.wav'
    with open(output_path, 'wb') as f:
        f.write(audio_out)
except Exception as e:
    print(e)

synth_stimulus([
    1.5,  # 1. Duration, percent
    0.1,  # 2. Tremolo rate, st
    0.05,  # 3. Tremolo depth, dB
    0,  # 4. Shift, semitones
    1,  # 5. Range, percent
    0,  # 6. Increase/Decrease, semitones
    0  # 7. Jitter, custom unit
], "example.wav", {
    'file': 'Harvard_L35_S01_0.wav'
})
